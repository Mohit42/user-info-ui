import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserInfoService } from '../user-info.service';

@Component({
  selector: 'app-display-user-info',
  templateUrl: './display-user-info.component.html',
  styleUrls: ['./display-user-info.component.scss']
})
export class DisplayUserInfoComponent implements OnInit {
  userInfo: any;

  constructor(
    private userInfoService: UserInfoService,
    private activatedRoute: ActivatedRoute  
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      const email = params.get('email');
      this.userInfoService.getUserData(email).subscribe(res => {
        if(res) {
          console.log(res);
          this.userInfo = res;
        }      
      });
    });
  }

}
