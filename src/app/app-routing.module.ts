import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayUserInfoComponent } from './display-user-info/display-user-info.component';
import { UserFormComponent } from './user-form/user-form.component';

const routes: Routes = [
  {  path: '', component : UserFormComponent },
  { path: 'user-info/:email', component:DisplayUserInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
