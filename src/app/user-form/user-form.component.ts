import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfoService } from '../user-info.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  constructor(
    private router: Router,
    private userInfoService: UserInfoService
    ) { }
  user: any = {
    place : 'Delhi'
  };
  places: Array<string> = ["Delhi", "Pune", "Mumbai"];

  ngOnInit(): void {
  }
  saveData() {
    this.userInfoService.saveUserData(this.user).subscribe(result => {
      if(result) {
        console.log(result);
        this.router.navigate([`/user-info/${this.user.email}`]);
      }
    });
  }

}
