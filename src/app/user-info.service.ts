import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  constructor(private http: HttpClient) {}
  url = 'http://localhost:4000/user';

  getUserData(email: any) {
    const params = '?email=' + `${email}`
    return this.http.get(this.url + params);
  }

  saveUserData(data: any) {
    return this.http.post(this.url, data);
  }
}